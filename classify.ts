
// interface UserInterface{
//     name: string;
//     roll:number
// }

// class User { 
    
//     constructor(protected name:string,public roll:number=9){}

//     getName(){
//         return `${this.name} ${this.roll}`;
//     }
// }

// let Abdul=new User("kabir",9);

// console.log(Abdul.getName())


class Person {
    // private ssn: string;
    // private firstName: string;
    // private lastName: string;

    constructor(public ssn: string, private firstName: string,public  lastName: string) {
        // this.ssn = ssn;
        // this.firstName = firstName;
        // this.lastName = lastName;
    }

    getFullName(): string {
        return `${this.firstName}`; 
    }
}


let person = new Person('153-07-3130', 'John', 'Doe');
 console.log(person.lastName);