"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var methDec = function () {
    return function (target, name, propertyDescriptor) {
        console.log(propertyDescriptor);
    };
};
var methodDec = /** @class */ (function () {
    function methodDec(name) {
        this.name = name;
    }
    methodDec.prototype.met = function (motu) {
        console.log(this.name);
    };
    __decorate([
        methDec()
    ], methodDec.prototype, "met", null);
    return methodDec;
}());
